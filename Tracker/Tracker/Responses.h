#pragma once
#include "Peer.h"

//https://wiki.theory.org/index.php/BitTorrentSpecification#Tracker_HTTP.2FHTTPS_Protocol
typedef struct PeerResponse 
{
	std::string failureReason{}; //optional
	std::string warningMessage{}; //optional
	unsigned int interval{}; //in seconds
	unsigned int minInterval{}; //optional
	std::string trackerID{};
	unsigned int complete{};
	unsigned int incomplete{};
	std::map<std::string, Peer> peers{};
	std::string peersBM{}; //binary model. NOT SUPPORTED (compact is always 0).
	PeerResponse() {};
}PeerResponse;

enum class ResponseCodes
{
	InvalidRequestType = 100,
	MissingInfoHash = 101,
	MissingPeerID = 102,
	MissingPort = 103,
	InvalidInfoHash = 150,
	InvalidPeerID = 151,
	InvalidNumwant = 152,
	InfoHashNotFound = 200, 
	SentBeforeTime = 500,
	GenericError = 900,
	Ok = 200
};

namespace FailureReason
{
	const std::map<ResponseCodes, const std::string> failureReasons
	{
		{ResponseCodes::InvalidRequestType, "Invalid request type."},
		{ResponseCodes::MissingInfoHash, "Missing info_hash."},
		{ResponseCodes::MissingPeerID, "Missing peer_id."},
		{ResponseCodes::MissingPort, "Missing port."},
		{ResponseCodes::InvalidInfoHash, "Invalid infohash: infohash is not 32 bytes long."},
		{ResponseCodes::InvalidPeerID, "peerid is not 20 bytes long."},
		{ResponseCodes::InvalidNumwant, "Invalid numwant."},
		{ResponseCodes::InfoHashNotFound, "info_hash not found in the database."},
		{ResponseCodes::SentBeforeTime, "Client sent an eventless request before the specified time."},
		{ResponseCodes::GenericError, "Generic error."}
	};

	const std::unordered_map<std::string, ResponseCodes> requestToFaliureReason
	{
		{"infoHash", ResponseCodes::InvalidInfoHash},
		{"peerID", ResponseCodes::InvalidPeerID},
		{"port", ResponseCodes::GenericError},
		{"uploaded", ResponseCodes::GenericError},
		{"downloaded", ResponseCodes::GenericError},
		{"left", ResponseCodes::GenericError},
		{"compact", ResponseCodes::GenericError},
		{"noPeerID", ResponseCodes::GenericError},
		{"event", ResponseCodes::GenericError},
		{"ip", ResponseCodes::GenericError},
		{"numwant", ResponseCodes::InvalidNumwant},
		{"key", ResponseCodes::GenericError},
		{"trackerID", ResponseCodes::GenericError}
	};
};