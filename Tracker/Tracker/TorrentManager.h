#pragma once
#include "Peer.h"

typedef std::string peerID;
typedef std::string infoHash;
typedef std::map<peerID, Peer> PeerList;
typedef std::map<infoHash, PeerList> TorrentList;

class TorrentManager //Singleton
{
public:
	static TorrentManager* getInstance(TorrentList& torrents) noexcept;
	TorrentManager(TorrentManager& other) = delete;
	void operator=(const  TorrentManager&) = delete;
	void saveTorrents() const noexcept;
	void loadTorrents() const noexcept;
private:
	TorrentManager(TorrentList& torrents);
	static TorrentManager* _Instance;
	TorrentList& _torrents;
	const std::string PATH = "Torrents.txt";
};

