#pragma once
#include "cpp-httplib-master/httplib.h"
#include "Requests.h"
#include "Responses.h"
#include <tuple>
#include "TorrentManager.h"

using namespace requests;

class Server //Singleton
{
public:
	static Server* getInstance() noexcept;
	void run();

	Server(Server& other) = delete;
	void operator=(const  Server&) = delete;

	static constexpr unsigned int MinPortNum = 1;
	static constexpr unsigned int MaxPortNum = 65535;
	static constexpr unsigned int TrackersNumwant = 200; //max torrents in this tracker.
	static constexpr unsigned int AnnounceMinInterval = 150; //2.5 minutes.
	static constexpr unsigned int AnnounceInterval = 300; //5 minutes.

private:
	Server();
	const std::string generateResponse(const httplib::Request& r, httplib::Response& res);
	const std::tuple<std::shared_ptr<PeerRequest>, ResponseCodes> isRequestValid(const httplib::Request& r) const noexcept;
	std::string toDictionaryResponse(PeerResponse& resStruct, bool noPeerID) const noexcept;

	constexpr static unsigned int _port = 17670;
	httplib::Server _svr;
	static Server* _Instance;
	TorrentList _torrents;
	TorrentManager* _torrentManager;
};