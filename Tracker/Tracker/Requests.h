#pragma once
#include <string>
#include <unordered_map>
#include <functional>

namespace requests
{
	enum class EventFieldOptions
	{
		NotSpecified,
		Started,
		Stopped,
		Completed
	};

	//https://wiki.theory.org/index.php/BitTorrentSpecification#Tracker_HTTP.2FHTTPS_Protocol
	typedef struct PeerRequest
	{
		std::string infoHash{};
		std::string peerID{};
		unsigned int port{};
		unsigned int uploaded{};
		unsigned int downloaded{};
		unsigned int left{};
		bool compact{}; //always 0
		bool noPeerID{};
		EventFieldOptions event{};
		std::string ip{}; //optional
		unsigned int numwant{}; //optional
		std::string key{}; //optional
		std::string trackerID{}; //optional
		PeerRequest() {};
	}PeerRequest;

	static constexpr unsigned int MinPortNum = 1;
	static constexpr unsigned int MaxPortNum = 65535;
	static constexpr unsigned int TrackersNumwant = 200; //max torrents in this tracker.


	bool isThirtyTwoBytesUrlEncodedStr(std::string_view str); //infoHash
	bool isTwentyBytesUrlEncodedStr(std::string_view str); //peerID
	bool isValidPort(std::string_view port); //port
	bool isInteger(std::string_view num); // uploaded, downloaded, left
	bool isBoolean(std::string_view num); //compact, noPeerID
	bool isValidEvent(std::string_view str); //event
	bool isValidIp(std::string_view ip); //ip
	bool isValidNumwant(std::string_view numwant); //numwant


	const std::unordered_map<std::string, std::function<bool(std::string_view)>> requestToValidator
	{
		{"infoHash", isThirtyTwoBytesUrlEncodedStr},
		{"peerID", isTwentyBytesUrlEncodedStr},
		{"port", isValidPort},
		{"uploaded", isInteger},
		{"downloaded", isInteger},
		{"left", isInteger},
		{"compact", isBoolean},
		{"noPeerID", isBoolean},
		{"event", isValidEvent},
		{"ip", isValidIp},
		{"numwant", isValidNumwant}
	};
};