#include "Requests.h"
#include "ip.h"
#include "cpp-httplib-master/httplib.h"
using namespace ip;

/// <summary>
/// This function check if the input string represents a thirty two bytes urlencoded string.
/// </summary>
/// <param name="str"> string to check.</param>
/// <returns> true if the string represents thirty two bytes url encoded string, false- otherwise.</returns>
bool requests::isThirtyTwoBytesUrlEncodedStr(std::string_view str)
{
	try
	{
		std::string tmp = httplib::detail::decode_url(str.data(), true);
		return tmp.length() == 32;
	}
	catch (...)
	{
		return false;
	}
}

/// <summary>
/// This function check if the input string represents a twenty bytes urlencoded string.
/// </summary>
/// <param name="str"> string to check.</param>
/// <returns> true if the string represents twenty bytes url encoded string, false- otherwise.</returns>
bool requests::isTwentyBytesUrlEncodedStr(std::string_view str)
{
	try
	{
		std::string tmp = httplib::detail::decode_url(str.data(), true);
		std::cout << tmp.length() << "\n";
		return tmp.length() == 20;
	}
	catch (...)
	{
		return false;
	}
}

/// <summary>
/// This function checks if the input param represents a valid port.
/// </summary>
/// <param name="port"> a string that represents a port.</param>
/// <returns> true if the string represents a valid port, false- otherwise.</returns>
bool requests::isValidPort(std::string_view port)
{
	try
	{
		int tmp = std::stoul(port.data(), 0, 10);
		return !(tmp < MinPortNum || tmp > MaxPortNum);
	}
	catch (...)
	{
		return false;
	}
}

/// <summary>
/// This function checks if the string represents an integer.
/// </summary>
/// <param name="num"> string to check.</param>
/// <returns>true if integer, false- otherwise.</returns>
bool requests::isInteger(std::string_view num)
{
	try
	{
		std::string str = num.data();
		return !str.empty() && std::find_if(str.begin(),
			str.end(), [](unsigned char c) { return !std::isdigit(c); }) == str.end();
	}
	catch (...)
	{
		return false;
	}
}

/// <summary>
/// This function checks if the input string represents a boolean num.
/// </summary>
/// <param name="num"> string to check.</param>
/// <returns> true if the string represents a boolean num, false- otherwise.</returns>
bool requests::isBoolean(std::string_view num)
{
	return (std::string)num.data() == "0" || (std::string)num.data() == "1";
}

/// <summary>
/// This function checks if the input string represents a valid value for the "event" field.
/// </summary>
/// <param name="str"> string to check.</param>
/// <returns> true if the input string represents a valid value for the "event" field, false- otherwise.</returns>
bool requests::isValidEvent(std::string_view str)
{
	return isBoolean(str) || (std::string)str.data() == "2" || (std::string)str.data() == "3";
}

/// <summary>
/// This function checks if the input string represents a valid IPV4 or IPV6.
/// </summary>
/// <param name="ip"> string to check.</param>
/// <returns> true if the input string represents a valid IPV4 or IPV6, false- otherwise.</returns>
bool requests::isValidIp(std::string_view ip)
{
	try
	{
		return std::regex_match(ip.data(), IPV4) || std::regex_match(ip.data(), IPV6);
	}
	catch (...)
	{
		return false;
	}
}

/// <summary>
/// This function checks if the input string represents a valid numwant.
/// </summary>
/// <param name="numwant"> string to check.</param>
/// <returns> true if the input string represents a valid numwant, false- otherwise.</returns>
bool requests::isValidNumwant(std::string_view numwant)
{
	try
	{
		int tmp = std::stoul(numwant.data(), 0, 10);
		return tmp <= TrackersNumwant;
	}
	catch (...)
	{
		return false;
	}
}