#include "Peer.h"

/// <summary>
/// This function generates an entry to the response dict.
/// </summary>
/// <param name="noPeerID"> if true peerID field wont be included, false- otherwise.</param>
/// <returns>An entry to the response dict.</returns>
std::map<std::string, bencode::data> Peer::toEntry(bool noPeerID)
{
	std::map<std::string, bencode::data> peerMap;
	if (!noPeerID)
		peerMap["id"] = bencode::data(this->peerID);
	peerMap["ip"] = bencode::data(this->ip);
	peerMap["port"] = bencode::data(this->port);

	return peerMap;
}