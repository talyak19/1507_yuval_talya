#ifndef THEWIDGETITEM_H
#define THEWIDGETITEM_H

#include <QWidget>
#include <QLineEdit>
#include <QPushButton>
#include <QSlider>
#include <QProgressBar>
#include <QLabel>
#include <TorrentHandler.h>

namespace Ui {
class TheWidgetItem;
}

class TheWidgetItem : public QWidget
{
    Q_OBJECT

public:
    explicit TheWidgetItem(QString label, const TorrentHandler* handler, QWidget *parent = 0);
    ~TheWidgetItem();

    void sendDownloadProgress();

private slots:
    void updateProgress(int progress);

private:
    Ui::TheWidgetItem *ui;
    const TorrentHandler* _handler;
    float _progress;
};

#endif // THEWIDGETITEM_H
