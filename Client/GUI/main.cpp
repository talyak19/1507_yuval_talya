#include "QMainWidget.h"
#include "GUI.h"
#include <QApplication>

int main(int argc, char *argv[])
{
    Client client;

    std::thread t(&Client::startConnection, std::ref(client));
    t.detach();

    QApplication a(argc, argv);
    GUI w(client);
    w.show();
    return a.exec();
}
