#include "GUI.h"
#include <qfiledialog.h>
#include <qmessagebox.h>
#include <thread>
#include <chrono>

GUI::GUI(Client& client, QWidget* parent)
	: QMainWindow(parent), _client(client), _torrentsList(new QMainWidget(*this))
{
    _ui.setupUi(this);
    _torrentsList->setObjectName("TorrentsList");
    _ui.verticalLayout->addWidget(_torrentsList);
}

void GUI::on_actionAdd_torrent_triggered()
{

    QString path = QFileDialog::getOpenFileName(this, tr("select a torrent"));
    //QString fileName = QFileDialog::getOpenFileName(this, tr("select a torrent"), tr("(*.torrent)"));

    const TorrentHandler* added = 0;
    try
    {
        added = _client.addTorrent(path.toStdString());
    }
    catch (...)
    {
        return;
    }

    _torrentsList->add(path, added);
}

GUI::~GUI()
{
    delete _torrentsList;
}


void GUI::on_actionGenerate_torrent_triggered()
{
    QString path = QFileDialog::getOpenFileName(this, tr("select a file"));

    //128KB
    int l = pow(2, 17);
    createTorrentFile(path.toStdString(), ".", l, "127.0.0.1:1234/announce");

}

