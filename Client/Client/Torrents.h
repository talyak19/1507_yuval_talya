#pragma once
#include <vector>
#include <string>
#include <openssl/sha.h>
#include <iostream>
#include <variant>
#include <coroutine>
#include "cppcoro/generator.hpp"

namespace Torrents
{
	static constexpr int hashLen = SHA256_DIGEST_LENGTH;
	static constexpr std::string_view TorrentFileExtention = ".torrent";

	struct TorrentInfo
	{
		int pieceLength;
		std::string pieces;
		std::string name;

		virtual int getPiecesCount() const = 0;
		virtual int getLastPieceIndex() const = 0;
		virtual int getLastPieceLength() const = 0;
		bool comparePiece(const char* piece, int pieceLen, int index) const;
	};

	struct SingleModeInfo : public TorrentInfo
	{
		long fileLength;

		virtual int getPiecesCount() const override;
		virtual int getLastPieceIndex() const override;
		virtual int getLastPieceLength() const override;

	};

	struct FileInfo
	{
		long length;
		std::string path;
	};

	struct MultiModeInfo : public TorrentInfo
	{
		std::vector<FileInfo> files;

		virtual int getPiecesCount() const override;
		virtual int getLastPieceIndex() const override;
		virtual int getLastPieceLength() const override;


	};

	enum class InfoMode
	{
		Single, Multi
	};

	struct Torrent
	{
		InfoMode mode;
		std::string trackerURL;
		std::shared_ptr<TorrentInfo> info;
	};

	cppcoro::generator<std::string_view> splitFilesToPieces(const std::vector<std::string>& fileNames, int pieceLen);

	std::shared_ptr<unsigned char[]> hash(const char* str, long length);
	std::string hashWithStr(std::string_view str);
	std::string splitAndHashStr(std::string_view str, int pieceLength);

	std::vector<std::string> splitPath(std::string_view str);
	std::string hashFile(std::string_view fileName, int pieceLength);
	std::string hashFiles(const std::vector<std::string>& fileNames, int pieceLength);

	void createTorrentFile(const std::string& src, const std::string& dst, int pieceLength, const std::string& trackerURL);

	Torrent readTorrentFile(const std::string& path);
	std::string generateTorrentHash(const std::string & info);
}