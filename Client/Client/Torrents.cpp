#include "Torrents.h"
#include <fstream>
#include <sstream>  
#include <iomanip>
#include "bencode.hpp"
#include <filesystem>

using namespace Torrents;

/// <summary>
/// Calculates the amount of pieces needed to downloaded the file based on
/// the file length and length of a piece
/// </summary>
int Torrents::SingleModeInfo::getPiecesCount() const
{
	return fileLength / pieceLength + (fileLength % pieceLength != 0);
}

int Torrents::SingleModeInfo::getLastPieceIndex() const
{
	return getPiecesCount() - 1;
}

int Torrents::SingleModeInfo::getLastPieceLength() const
{
	int remainder = fileLength % pieceLength;
	return remainder == 0 ? pieceLength : remainder;
}

/// <summary>
/// Checks if a downloaded piece is the same as the piece in the torrent file
/// </summary>
/// <param name="info">The torrent info</param>
/// <param name="piece">The piece that was downloaded</param>
/// <param name="index">The index of the piece</param>
/// <returns>True if the piece is the same </returns>
bool Torrents::TorrentInfo::comparePiece(const char* piece, int pieceLen, int index) const
{
	std::string_view correctPiece(this->pieces.c_str() + index * hashLen, hashLen);

	//Hash the downloaded piece
	auto hashed = hash(piece, pieceLen);

	for (int i = 0; i < hashLen; i++)
	{
		if ((char)hashed[i] != correctPiece[i])
		{
			return false;
		}
	}

	return true;

}


int Torrents::MultiModeInfo::getPiecesCount() const
{
	int len = 0;
	for (auto& f : files)
	{
		len += f.length;
	}

	return len / pieceLength + (len % pieceLength != 0);;
}

int Torrents::MultiModeInfo::getLastPieceIndex() const
{
	return getPiecesCount() - 1;
}

int Torrents::MultiModeInfo::getLastPieceLength() const
{
	int len = 0;
	for (auto& f : files)
	{
		len += f.length;
	}

	int remainder = len % pieceLength;
	return remainder == 0 ? pieceLength : remainder;
}


/// <summary>
/// Generator that gets list of files and splits them into pieces.
/// All files are considered as one long file
/// </summary>
/// <param name="fileNames">Vector of file names</param>
/// <param name="pieceLength">Length of each piece</param>
/// <returns>String that contains the current piece</returns>
cppcoro::generator<std::string_view> Torrents::splitFilesToPieces(const std::vector<std::string>& fileNames, int pieceLength)
{
	std::string allPieces;
	std::string remainder;

	for (int i = 0; i < fileNames.size(); i++)
	{
		std::ifstream srcFile(fileNames[i], std::ifstream::binary);

		if (!(srcFile.is_open()))
		{
			throw std::exception("Couldn't open file");
		}

		srcFile.seekg(0, srcFile.end);
		long fileLength = srcFile.tellg();
		srcFile.seekg(0, srcFile.beg);

		std::unique_ptr<char> buf(new char[pieceLength]);

		//Get amount of pieces without considering non-full pieces
		long lengthWithRemainder = fileLength + remainder.length();
		int wholePiecesCount = lengthWithRemainder / pieceLength;

		for (int i = 0; i < wholePiecesCount; i++)
		{
			std::string readPart;
			if (!remainder.empty())
			{
				std::string combinedPart;

				//Might have remainder from multiple files which can result in
				//a length longer than piece length
				if (remainder.length() >= pieceLength)
				{
					//Take enough to make piece and remove
					//that part from remainder
					combinedPart = remainder.substr(0, pieceLength);
					remainder.erase(0, pieceLength);
				}
				else
				{
					int lengthToCompletePiece = pieceLength - remainder.length();
					srcFile.read(buf.get(), lengthToCompletePiece);
					combinedPart = remainder + std::string(buf.get(),
						lengthToCompletePiece);

					remainder.clear();

				}

				readPart = combinedPart;
			}
			else
			{
				srcFile.read(buf.get(), pieceLength);
				readPart = std::string(buf.get(), pieceLength);
			}

			co_yield readPart;
		}


		long curPos = srcFile.tellg();
		long remainderLength = fileLength - curPos;

		if (remainderLength == 0)
		{
			srcFile.close();
		}
		else
		{
			srcFile.read(buf.get(), remainderLength);
			srcFile.close();

			remainder += std::string(buf.get(), remainderLength);
		}

	}

	std::string cur;
	while (!remainder.empty())
	{
		if (remainder.length() >= pieceLength)
		{
			cur = remainder.substr(0, pieceLength);
			remainder = remainder.substr(pieceLength);
		}
		else
		{
			cur = remainder;
			remainder = "";
		}
		co_yield cur;
	}
	
}

std::shared_ptr<unsigned char[]> Torrents::hash(const char* str, long length)
{
	auto hashed = std::make_shared<unsigned char[]>(hashLen);
	SHA256(reinterpret_cast<unsigned char const*>(str),
		length,
		hashed.get());
	return hashed;
}

std::string Torrents::hashWithStr(std::string_view str)
{
	auto piecePtr = hash(str.data(), str.length());
	std::string hashed = std::string(reinterpret_cast<char*>(piecePtr.get()),
		hashLen);

	return hashed;
}

std::string Torrents::splitAndHashStr(std::string_view targetStr, int pieceLength)
{
	std::string pieces;
	std::string cur;

	while (targetStr.length() > 0)
	{
		if (targetStr.length() >= pieceLength)
		{
			cur = targetStr.substr(0, pieceLength);
			targetStr = targetStr.substr(pieceLength);
		}
		else
		{
			cur = targetStr;
			targetStr = "";
		}

		pieces += hashWithStr(cur);
	}

	return pieces;
}

/// <summary>
/// Splits string by the system path separator into a vector
/// </summary>
/// <param name="str">String to split</param>
/// <returns>Vector containing the parts</returns>
std::vector<std::string> Torrents::splitPath(std::string_view str)
{
	auto token = std::filesystem::path::preferred_separator;
	std::vector<std::string> result;

	while (str.size())
	{
		size_t index = str.find(token);
		if (index != std::string::npos)
		{
			result.push_back(std::string(str.substr(0, index)));
			str = str.substr(index + 1);
			if (str.size() == 0)
			{
				result.push_back(str.data());
			}
		}
		else
		{
			result.push_back(str.data());
			str = "";
		}
	}

	return result;
}

/// <summary>
/// Breaks file into equal pieces each with the length pieceLength and
/// hashes each piece. Last piece may have a different size
/// </summary>
/// <param name="fileName">The path to the file</param>
/// <param name="pieceLength">The length of each piece</param>
/// <returns>A string containing all of the hashed pieces</returns>
std::string Torrents::hashFile(std::string_view fileName, int pieceLength)
{
	std::ifstream srcFile(fileName.data(), std::ifstream::binary);

	if (!(srcFile.is_open()))
	{
		throw std::exception("Couldn't open file");
	}

	srcFile.seekg(0, srcFile.end);
	long fileLength = srcFile.tellg();
	srcFile.seekg(0, srcFile.beg);

	//Hash each piece and add it to string
	std::string pieces;
	std::unique_ptr<char> buf(new char[pieceLength]);
	auto hashPiece = [&](int length)
	{
		srcFile.read(buf.get(), length);
		pieces += std::string(reinterpret_cast<char*>(hash(buf.get(), length).get()), hashLen);
	};

	int piecesCount = fileLength / pieceLength;
	for (int i = 0; i < piecesCount; i++)
	{
		hashPiece(pieceLength);
	}

	//Hash last piece with different size
	int lastPieceSize = fileLength - piecesCount * pieceLength;
	hashPiece(lastPieceSize);

	srcFile.close();

	return pieces;
}

std::string Torrents::hashFiles(const std::vector<std::string>& fileNames, int pieceLength)
{
	std::string allPieces;

	for (auto str : splitFilesToPieces(fileNames, pieceLength))
	{
		allPieces += hashWithStr(str);
	}

	return allPieces;
}

/// <summary>
/// Gets a file and creates a torrent file using it
/// Hashes using SHA256
/// </summary>
/// <param name="src">The file to create torrent file of</param>
/// <param name="dst">The path to put the torrent file in</param>
/// <param name="pieceLength">The length of each piece</param>
/// <param name="trackerURL">The url of the tracker</param>
void Torrents::createTorrentFile(const std::string& src, const std::string& dst, int pieceLength, const std::string& trackerURL)
{

	std::string dstFilename;
	if (std::filesystem::is_directory(dst))
	{
		//Take the name of the torrent file from the source file
		dstFilename = std::filesystem::path(src).filename().string() + ".torrent";
	}
	else
	{
		//Use the filename specified by the user
		dstFilename = std::filesystem::path(dst).filename().string();
		if (!dstFilename.contains(TorrentFileExtention))
		{
			dstFilename += TorrentFileExtention;
		}
	}

	std::ofstream dstFile(dstFilename, std::ofstream::binary);
	if (!dstFile.is_open())
	{
		throw std::exception();
	}

	std::string pieces;
	bencode::dict info;

	if (std::filesystem::is_directory(src))
	{
		bencode::list files;
		std::vector<std::string> allPaths;

		for (auto& p : std::filesystem::recursive_directory_iterator(src))
		{
			if (p.is_directory())
			{
				continue;
			}

			allPaths.push_back(p.path().string());

			auto length = p.file_size();
			std::string pathStr = std::filesystem::relative(p,
				src).make_preferred().string();
			bencode::list path;
			for (auto& part : splitPath(pathStr))
			{
				path.push_back(part);
			}

			bencode::dict fileDict = {
				{"length", length},
				{"path", path}
			};

			files.push_back(fileDict);
		}

		pieces = hashFiles(allPaths, pieceLength);

		info =
		{
			{"piece length", pieceLength},
			{"pieces", pieces},
			{"files", files},
			{"name", std::filesystem::path(src).filename().string()}
		};
	}
	else
	{
		std::string pieces = hashFile(src, pieceLength);
		auto length = std::filesystem::file_size(src);
		info =
		{
			{"piece length", pieceLength},
			{"pieces", pieces},
			{"length", length},
			{"name", std::filesystem::path(src).filename().string()}
		};

	}

	bencode::encode
	(
		dstFile,
		bencode::dict{
		{"announce", trackerURL},
		{"info", info}
		}
	);

	dstFile.close();
}

/// <summary>
/// Opens a torrent file and reads it into a Torrent struct
/// </summary>
/// <param name="path">The path of the torrent file</param>
/// <returns>Torrent struct created using the file</returns>
Torrent Torrents::readTorrentFile(const std::string& path)
{
	Torrent torrent;

	std::ifstream tFile(path, std::ifstream::binary);
	auto data = bencode::decode(tFile);
	auto contents = boost::get<bencode::dict>(data);

	torrent.trackerURL = boost::get<bencode::string>(contents["announce"]);

	auto info = boost::get<bencode::dict>(contents["info"]);

	if (info.count("files"))
	{
		torrent.mode = InfoMode::Multi;
		MultiModeInfo multiInfo;

		multiInfo.pieceLength = boost::get<bencode::integer>(info["piece length"]);
		multiInfo.pieces = boost::get<bencode::string>(info["pieces"]);
		multiInfo.name = boost::get<bencode::string>(info["name"]);

		bencode::list files = boost::get<bencode::list>(info["files"]);
		for (auto& entry : files)
		{
			auto dict = boost::get<bencode::dict>(entry);

			//Format path list into a string
			bencode::list path = boost::get<bencode::list>(dict["path"]);
			std::string pathStr;
			for (auto& part : path)
			{
				std::string partStr = boost::get<bencode::string>(part);
				pathStr += partStr +
					(char)std::filesystem::path::preferred_separator;
			}
			pathStr.pop_back(); //Remove last separator

			FileInfo fInfo;
			fInfo.path = pathStr;
			fInfo.length = boost::get<bencode::integer>(dict["length"]);
			multiInfo.files.push_back(fInfo);
		}

		torrent.info = std::make_shared<MultiModeInfo>(multiInfo);

	}
	else
	{
		torrent.mode = InfoMode::Single;
		SingleModeInfo singleInfo;

		singleInfo.pieceLength = boost::get<bencode::integer>(info["piece length"]);
		singleInfo.pieces = boost::get<bencode::string>(info["pieces"]);
		singleInfo.name = boost::get<bencode::string>(info["name"]);
		singleInfo.fileLength = boost::get<bencode::integer>(info["length"]);

		torrent.info = std::make_shared<SingleModeInfo>(singleInfo);
	}


	tFile.close();
	return torrent;
}


std::string Torrents::generateTorrentHash(const std::string& torrentPath)
{
	std::ifstream tFile(torrentPath, std::ifstream::binary);
	auto data = bencode::decode(tFile);
	auto contents = boost::get<bencode::dict>(data);

	auto info = boost::get<bencode::dict>(contents["info"]);
	std::string bencodedInfo = bencode::encode(info);
	auto hashed = hash(bencodedInfo.c_str(), bencodedInfo.length());

	return std::string(reinterpret_cast<char*>(hashed.get()), hashLen);
}

