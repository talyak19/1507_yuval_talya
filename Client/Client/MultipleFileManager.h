#pragma once
#include "FileManager.h"
class MultipleFileManager :
    public FileManager
{
public:
	MultipleFileManager(std::string_view path, const MultiModeInfo* info);

	virtual void write(int position, const std::vector<unsigned char>& content) override;
	virtual std::vector<unsigned char> read(int position, int length) override;
	virtual void updateBitfield() override;

private:

	struct FileIndexAndPosition
	{
		int fIndex;
		long position;
	};
	FileIndexAndPosition getFileIndexAndPosition(int position);
	int fileLenAtIndex(int index) const;

	const MultiModeInfo _info;
	std::vector<std::string> fullFilePaths;

};

