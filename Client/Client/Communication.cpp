#include "Communication.h"

using namespace Communication;

int Communication::sendToSocket(SOCKET peer, const std::vector<unsigned char>& msg)
{
	int res = 0;
	if ((res = send(peer, (char*)msg.data(), (int)msg.size(), 0)) == SOCKET_ERROR)
	{
		printf("send failed with error: %d\n", WSAGetLastError());
		throw std::exception();
	}

	return res;
}

std::shared_ptr<char> Communication::recvFromSocket(SOCKET sock, int bytesAmount)
{
	int res = 0;
	std::shared_ptr<char> buffer(new char[bytesAmount]);
	res = recv(sock, buffer.get(), bytesAmount, 0);
	if (res < 0)
	{
		printf("recv failed with error: %d\n", WSAGetLastError());
		throw std::exception();
	}

	return buffer;
}




void Communication::sendHandshake(SOCKET peer, std::string_view hash, std::string_view id, std::string_view ip, std::string_view port)
{
	HandshakeMessage msg;
	msg.infoHash = hash;
	msg.peerId = id;
	MessageSerializer serializer(msg);
	auto& serializedMsg = serializer.data;

	try
	{
		int res = sendToSocket(peer, serializedMsg);

		std::cout << std::format("{} Bytes sent in Handshake message to"
			" peer {} with ip {} and port {}\n",
			res, peer, ip, port);
	}
	catch (...)
	{
		std::cout << std::format("Error in Handshake message to"
			" peer {} with ip {} and port {}\n",
			peer, ip, port);
	}
}

HandshakeMessage Communication::receiveHandshake(SOCKET sock)
{
	int length = HandshakeMessage::length;
	auto buffer = recvFromSocket(sock, length);

	std::vector<unsigned char> msgContents(buffer.get(), buffer.get() + length);
	MessageDeserializer<HandshakeMessage> deserializer(msgContents);

	std::cout << std::format("{} Bytes received in Handshake message from"
		" sock {}\n",
		deserializer.message.length, sock);

	return deserializer.message;
}


/// <summary>
/// Polls the given sockets using the given flags and
/// returns after minimal waiting
/// </summary>
/// <param name="socketsToCheck">The sockets to poll</param>
/// <param name="events">The event to check</param>
/// <returns>A pair of the sockets that can contain the event and
/// sockets that errors occured on</returns>
std::shared_ptr<PollResults> Communication::pollSockets(const std::vector<SOCKET>& socketsToCheck, int events)
{
	return pollSockets(socketsToCheck, events, 1);;
}

/// <summary>
/// Polls the given sockets using the given flags 
/// </summary>
/// <param name="socketsToCheck">The sockets to poll</param>
/// <param name="events">The event to check</param>
/// <param name="waitTime">Time to wait</param>
/// <returns>A pair of the sockets that can contain the event and
/// sockets that errors occured on</returns>
std::shared_ptr<PollResults> Communication::pollSockets(const std::vector<SOCKET>& socketsToCheck, int events, int waitTime)
{
	auto results = std::make_shared<PollResults>();
	if (socketsToCheck.size() == 0)
	{
		return nullptr;
	}

	std::vector<WSAPOLLFD> pollFdVec;

	WSAPOLLFD currendPollFd{};
	currendPollFd.events = events;
	currendPollFd.revents = 0;
	for (auto socket : socketsToCheck)
	{
		currendPollFd.fd = socket;
		pollFdVec.emplace_back(currendPollFd);
	}

	if (WSAPoll(pollFdVec.data(), (ULONG)socketsToCheck.size(), waitTime) < 0)
	{
		printf("poll failed with error: %d\n", WSAGetLastError());
		throw std::exception();
	}


	for (auto& fd : pollFdVec)
	{
		if (fd.revents & events)
		{
			results.get()->eventFulfillingSockets.emplace_back(fd.fd);
		}
		else if ((fd.revents & POLLERR) ||
			(fd.revents & POLLHUP) ||
			(fd.revents & POLLNVAL))
		{
			results.get()->badSockets.emplace_back(fd.fd);
		}

	}

	return results;
}


/// <summary>
/// Checks for sockets that can be read without blocking and 
/// returns after minimal waiting
/// </summary>
/// <param name="socketsToCheck">Sockets to check</param>
/// <returns>A pair of the sockets that can be read and
///  sockets that errors occured on</returns>
std::shared_ptr<PollResults> Communication::getReadableSockets(const std::vector<SOCKET>& socketsToCheck)
{
	return pollSockets(socketsToCheck, POLLRDNORM);
}

/// <summary>
/// Checks for sockets that can be read without blocking
/// </summary>
/// <param name="socketsToCheck">Sockets to check</param>
/// <param name="waitTime">Time to wait, less than 0 to wait indefinitely</param>
/// <returns>A pair of the sockets that can be written to and 
/// sockets that errors occured on</returns>
std::shared_ptr<PollResults> Communication::getReadableSockets(const std::vector<SOCKET>& socketsToCheck, int waitTime)
{
	return pollSockets(socketsToCheck, POLLRDNORM, waitTime);
}

/// <summary>
/// Checks for sockets that can be written to without blocking and 
/// returns after minimal waiting
/// </summary>
/// <param name="socketsToCheck">Sockets to check</param>
/// <returns>A pair of the sockets that can be written to and 
/// sockets that errors occured on</returns>
std::shared_ptr<PollResults> Communication::getWriteableSockets(const std::vector<SOCKET>& socketsToCheck)
{
	return pollSockets(socketsToCheck, POLLWRNORM);
}

/// <summary>
/// Checks for sockets that can be written to without blocking
/// </summary>
/// <param name="socketsToCheck">Sockets to check</param>
/// <param name="waitTime">Time to wait, less than 0 to wait indefinitely</param>
/// <returns>A pair of the sockets that can be written to and 
/// sockets that errors occured on</returns>
std::shared_ptr<PollResults> Communication::getWriteableSockets(const std::vector<SOCKET>& socketsToCheck, int waitTime)
{
	return pollSockets(socketsToCheck, POLLWRNORM, waitTime);

}
