#pragma once
#include "FileManager.h"

class SingleFileManager :
    public FileManager
{
public:
    SingleFileManager(std::string_view path, const SingleModeInfo* info);
    ~SingleFileManager();

	virtual void write(int position, const std::vector<unsigned char>& content) override;
	virtual std::vector<unsigned char> read(int position, int length) override;
	virtual void updateBitfield() override;


private:
	const SingleModeInfo _info;
	std::fstream _targetFile;

};

