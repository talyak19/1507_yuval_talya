#pragma once

#undef UNICODE

#define WIN32_LEAN_AND_MEAN

#include <windows.h>
#include <winsock2.h>
#include <ws2tcpip.h>
#include <format>
#include <iostream>
#include <string>
#include <string_view>
#include <exception>
#include <vector>
#include <thread>
#include <mutex>
#include <map>
#include <queue>
#include <fstream>
#include "Exceptions.h"
#include "Messages.h"
#include "Torrents.h"
#include "FileManager.h"
#include "Communication.h"
#include "Bitsets.h"
#include "Messages.h"
// Need to link with Ws2_32.lib
#pragma comment (lib, "Ws2_32.lib")

// #pragma comment (lib, "Mswsock.lib")

using namespace Bitsets;
using namespace Messages;
using namespace Communication;

class SocketPtr
{
	//SocketPtr
private:
	int count;
	SOCKET _sock;
};

class Peer
{
public:

	Peer(SOCKET sock, std::string_view id, int bitfieldSize);
	~Peer();

	SOCKET getSocket() const noexcept;
	std::string_view getId() const noexcept;

	const DynamicBitset& getBitfield() const noexcept;

	std::pair<MessageType, std::vector<unsigned char>> receiveMessage() const;

	template <Message Msg>
	int sendMessage(Msg& msg) const;
	int sendMessage(MessageType type, std::vector<unsigned char> msg) const;
	int sendBitfield(DynamicBitset bitfield);
	int sendRequest(int index, int begin, int length);
	int sendHaveRequest();

	void updateBitfieldMap(const std::vector<unsigned char>& bitfield) noexcept;
	void updateBitfieldMap(int index) noexcept;
	
	void addReuqest(RequestMessage& req);
	void removeRequest(const std::shared_ptr<RequestMessage> req);

	const std::vector<std::shared_ptr<RequestMessage>>& getRequests() const;
	int getPendingRequestCount() const;
	void decreasePendingRequestsCount() ;

	void addPieceToInform(int index);
	int getUninformedPiecesCount() const;

	bool wasSentBitfield() const noexcept;
private:

	const SOCKET _socket;
	DynamicBitset _bitfield;
	std::string _id;

	//Number of requests that were sent to this peer
	int _pendingRequests;
	//Requests that this peer sent
	std::vector<std::shared_ptr<RequestMessage>> _requests;

	bool _wasSentBitfield;
	std::queue<int> _uninformedPiecesIndices;
};


/// <summary>
/// Serializes a message and sends it to the given peer
/// </summary>
/// <param name="msg">The message to send</param>
/// <returns>Amount of bytes sent</returns>
template<Message Msg>
inline int Peer::sendMessage(Msg& msg) const
{
	MessageSerializer<Msg> serializer(msg);
	return sendMessage(msg.id, serializer.data);
}