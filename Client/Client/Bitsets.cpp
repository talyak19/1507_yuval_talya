#include "Bitsets.h"
#include <algorithm>

/// <summary>
/// Will take a vector of bits and convert it to a vector of bytes
/// </summary>
/// <param name="bitfield">The vector of bits to convert</param>
/// <returns>The created vector of bytes, spare bits at the end are set to zero </returns>
std::vector<unsigned char> Bitsets::convertFromBitfield(DynamicBitset bitfield) noexcept
{
	unsigned char curByte = 0;
	bool bitsToConvert[_ByteSize] = { 0 };
	size_t curBit = 0;
	std::vector<unsigned char> res;

	while (curBit < bitfield.size())
	{
		//Take the current 8 bits and put them in an array for an easier conversion
		for (int i = 0; i < _ByteSize && curBit < bitfield.size(); i++)
		{
			bitsToConvert[i] = bitfield[curBit];
			curBit++;
		}

		//Use the 8 bits in the array and convert them to a byte
		for (int i = _ByteSize - 1; i >= 0; i--)
		{
			curByte = unsigned char((bitsToConvert[_ByteSize - 1 - i] << i) | curByte);
		}
		res.push_back(curByte);

		//Reset the temps back to 0
		curByte = 0;
		std::ranges::fill(bitsToConvert, 0);
	}

	return res;
}