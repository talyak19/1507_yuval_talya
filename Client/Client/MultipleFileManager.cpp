#include "MultipleFileManager.h"
#include <deque>

MultipleFileManager::MultipleFileManager(std::string_view path, const MultiModeInfo* info) :
	_info(*info), FileManager(path, &_info)
{
	_piecesBitfield.resize(info->getPiecesCount());

	//Create the directories that the given path contains and a directory for
	//each file within the torrent info inside the given path if needed
	std::filesystem::create_directories(path);
	for (auto& entry : info->files)
	{
		std::filesystem::path fullPath = std::filesystem::path(path) /
			std::filesystem::path(entry.path);
		fullFilePaths.push_back(fullPath.string());

		if (!std::filesystem::exists(fullPath))
		{
			std::filesystem::create_directories(fullPath.parent_path());
			std::ofstream(fullPath).close(); //Create empty file
		}
	}

	updateBitfield();
}

/// <summary>
/// Finds the file and position within the file that a 
/// non-boundaries considering position is pointing to
/// </summary>
/// <param name="position">The non-boundaries considering position</param>
/// <returns>The file index and poisition within it</returns>
MultipleFileManager::FileIndexAndPosition MultipleFileManager::getFileIndexAndPosition(int position)
{
	FileIndexAndPosition fIndexNPos{};

	long curPosition = 0;
	for (int i = 0; i < _info.files.size(); i++)
	{
		//Check if the given position is within the boundaries of 
		//the current file
		if (position < curPosition + _info.files[i].length)
		{
			fIndexNPos.fIndex = i;
			fIndexNPos.position = position - curPosition;
			break;
		}

		curPosition += _info.files[i].length;
	}

	return fIndexNPos;
}

int MultipleFileManager::fileLenAtIndex(int index) const
{
	return  _info.files[index].length;
}

/// <summary>
/// Writes block into file. May also write to multiple files if needed
/// </summary>
/// <param name="position">The position to starting writing to</param>
/// <param name="content">The bytes to write into the file(s)</param>
void MultipleFileManager::write(int position, const std::vector<unsigned char>& content)
{
	std::vector<unsigned char> contentLeft;
	int amountWritten = 0;

	auto fIndexNPos = getFileIndexAndPosition(position);
	long startPos = fIndexNPos.position;
	int startFileIndex = fIndexNPos.fIndex;

	int amountToCompleteFile = fileLenAtIndex(startFileIndex) - startPos;
	//Check if piece goes over multiple files
	int length = content.size() > amountToCompleteFile ?
		amountToCompleteFile : content.size();

	std::fstream file(fullFilePaths[startFileIndex],
		std::fstream::in | std::fstream::out | std::fstream::binary);
	file.seekp(startPos, file.beg);
	file.write((char*)content.data(), length);
	file.close();

	if (content.size() > amountToCompleteFile)
	{
		contentLeft = std::vector(content.begin() + amountToCompleteFile,
			content.end());

		int i = startFileIndex + 1;
		//Start writing piece to multiple files
		while (contentLeft.size() > 0)
		{
			std::fstream file(fullFilePaths[i], std::fstream::in | std::fstream::out | std::fstream::binary);

			//Write full file if given content contains its full data
			int length = fileLenAtIndex(i) < contentLeft.size() ?
				fileLenAtIndex(i) : contentLeft.size();

			file.write((char*)contentLeft.data(), length);
			file.close();

			contentLeft = std::vector(contentLeft.begin() + length,
				contentLeft.end());

			i++;
		}
	}

}

/// <summary>
/// Reads from a file an amount starting from the given position
/// May also read from multiple files if needed
/// </summary>
/// <param name="position">The position to start reading from</param>
/// <param name="pieceLen">The amount to read</param>
/// <returns>The data read from the file(s)</returns>
std::vector<unsigned char> MultipleFileManager::read(int position, int pieceLen)
{
	std::vector<unsigned char> readData;

	auto fIndexNPos = getFileIndexAndPosition(position);
	long startPos = fIndexNPos.position;
	int startFileIndex = fIndexNPos.fIndex;

	int remainingFileData = fileLenAtIndex(startFileIndex) - startPos;
	//Check if current file doesn't contain enough data for a piece
	int length = pieceLen > remainingFileData ? remainingFileData : pieceLen;
	readData.resize(length);

	std::ifstream file(fullFilePaths[startFileIndex], std::ifstream::binary);
	file.seekg(startPos, file.beg);
	file.read((char*)readData.data(), pieceLen);
	file.close();

	//Check if reading from multiple files is required
	if (pieceLen > remainingFileData)
	{
		std::unique_ptr<char[]> buffer(new char[pieceLen - remainingFileData]);

		int amountLeft = pieceLen - remainingFileData;
		int i = startFileIndex + 1;
		while (readData.size() < pieceLen)
		{
			//Check if file contains enough bytes to complete piece
			int amountToRead = fileLenAtIndex(i) > amountLeft ?
				amountLeft : fileLenAtIndex(i);

			std::ifstream file(fullFilePaths[i], std::ifstream::binary);
			file.read(buffer.get(), amountToRead);
			file.close();

			readData.insert(readData.end(),
				buffer.get(),
				buffer.get() + amountToRead);

			amountLeft -= amountToRead;
			i++;
		}
	}

	return readData;
}

/// <summary>
/// Goes through all files and validates their data
/// </summary>
void MultipleFileManager::updateBitfield()
{
	int i = 0;
	for (auto str : splitFilesToPieces(fullFilePaths, _info.pieceLength))
	{
		_piecesBitfield[i] = _info.comparePiece(str.data(), str.length(), i);
		i++;
	}

}

