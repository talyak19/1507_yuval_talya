#include "SingleFileManager.h"

SingleFileManager::SingleFileManager(std::string_view path, const SingleModeInfo* info) :
	_info(*info), FileManager(path, &_info)
{
	_piecesBitfield.resize(info->getPiecesCount());

	if (std::filesystem::exists(path))
	{
		_targetFile.open
		(
			_targetPath,
			std::fstream::in | std::fstream::out | std::fstream::binary
		);

		updateBitfield();

		//Reopen without write privilege if seeding
		if (finishedDownloading())
		{
			_targetFile.close();
			_targetFile.open
			(
				_targetPath,
				std::fstream::in | std::fstream::binary
			);
		}
	}
	else
	{
		//Create file
		std::ofstream(path.data()).close();

		_targetFile.open
		(
			_targetPath,
			std::fstream::in | std::fstream::out | std::fstream::binary
		);
	}

}

SingleFileManager::~SingleFileManager()
{
	_targetFile.close();
}

/// <summary>
/// Writes into the file and flushes the buffer
/// </summary>
/// <param name="position">The position to starting writing to</param>
/// <param name="content">The bytes to write into the file</param>
void SingleFileManager::write(int position, const std::vector<unsigned char>& content)
{
	_targetFile.seekp(position);
	_targetFile.write((char*)content.data(), content.size());
	_targetFile.flush();
}

/// <summary>
/// Reads from a file an amount starting from the given position
/// </summary>
/// <param name="position">The position to start reading from</param>
/// <param name="length">The amount to read</param>
/// <returns>The data read from the file</returns>
std::vector<unsigned char> SingleFileManager::read(int position, int length)
{
	std::vector<unsigned char> res;

	_targetFile.seekg(position, _targetFile.beg);
	int leftToRead = _info.fileLength - _targetFile.tellg();
	if (leftToRead < length)
	{
		length = leftToRead;
	}

	res.resize(length);
	_targetFile.read((char*)res.data(), length);

	if (_targetFile.bad()) {
		perror("stream badbit. error state");
	}

	return res;
}


/// <summary>
/// Goes through the open file and check which pieces were downloaded.
/// Downloaded pieces are set to 1 in the bitfield
/// </summary>
/// <param name="pieceLength">Length of a single piece</param>
void SingleFileManager::updateBitfield()
{
	//Go through the file and check each piece in it
	std::unique_ptr<char[]> buffer(new char[_info.pieceLength]);
	for (int i = 0; i < _info.getLastPieceIndex(); i++)
	{
		_targetFile.read(buffer.get(), _info.pieceLength);
		_piecesBitfield[i] = _info.comparePiece(buffer.get(), _info.pieceLength ,i);

		if (_targetFile.bad()) {
			perror("stream badbit. error state");
		}
	}

	//Check the last piece with its different length
	_targetFile.read(buffer.get(), _info.getLastPieceLength());
	_piecesBitfield[_info.getLastPieceIndex()] = 
		_info.comparePiece(buffer.get(), 
			_info.getLastPieceLength(),
			_info.getLastPieceIndex());


	if (_targetFile.bad()) {
		perror("stream badbit. error state");
	}

	_targetFile.clear();

}
