#include "Exceptions.h"


char const* MaxPeersReached::what() const noexcept
{
    return "The maximum amount of peers has been reached";
}

PeerConnectionError::PeerConnectionError(SOCKET socket) noexcept :
    _sock(socket)
{
}

PeerConnectionError::PeerConnectionError(SOCKET sock, std::string_view id) noexcept :
    _sock(sock), _id(id)
{
}

char const* PeerConnectionError::what() const noexcept
{
    return "An error occured while handling peer";
}

int PeerConnectionError::getSocket() const noexcept
{
    return _sock;
}

std::string_view PeerConnectionError::getId()
{
    return _id;
}


char const* PeerAlreadyKnown::what() const noexcept
{
    return "An attempted connection to an already connected peer failed";
}

char const* UnknownPeer::what() const noexcept
{
    return nullptr;
}

EstablishConnectionError::EstablishConnectionError(std::string_view ip, 
    std::string_view port) : _ip(ip), _port(port)
{
}

char const* EstablishConnectionError::what() const noexcept
{
    return "Unable to establish connection with peer";
}

std::string_view EstablishConnectionError::getIp() const noexcept
{
    return _ip;
}

std::string_view EstablishConnectionError::getPort() const noexcept
{
    return _port;
}
