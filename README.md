# Sync++
Sync++ is a BitTorrent-inspired software that allows you to share files.

## Description
This project contains a tracker server and a client-side. By running the server and several clients simultaneously clients can find each other using the server and share files with each other. Each client can add files and/ or download files from other clients.

## Installation

Go to [Releases](https://gitlab.com/talyak19/1507_yuval_talya/-/releases) and download client and tracker files.

## Usage
Firstly, open the tracker server to allow users to find each other and then open the client and add your torrent files.


Client Usage:

![](Images/client_usage.png)

1 - Adds a .torrent file and start downloading. <br>
2 - Generate a .torrent file that can be shared with other users to download the file.  <br>
3 - Delete an active torrent from the list.
